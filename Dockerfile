# VERSION 0.11
# AUTHOR:         Jiaji Yin <yinjiaji110@gmail.com>
# DESCRIPTION:    Image with DokuWiki & lighttpd
# TO_BUILD:       docker build -t jiaji/dokuwiki .
# TO_RUN:         docker run -d -p 80:80 --name my_wiki jiaji/dokuwiki


FROM ubuntu:16.04
MAINTAINER Jiaji Yin <yinjiaji110@gmail.com>

# Set the version you want of dokuwiki
# version at https://download.dokuwiki.org/archive
ENV DOKUWIKI_VERSION 2017-02-19e
ENV DOKUWIKI_CSUM 09bf175f28d6e7ff2c2e3be60be8c65f

ENV LAST_REFRESHED 16. August 2017

# Update & install packages & cleanup afterwards
RUN DEBIAN_FRONTEND=noninteractive \
    apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install wget lighttpd php7.0-fpm php7.0 php7.0-xml && \
    apt-get clean autoclean && \
    apt-get autoremove && \
    rm -rf /var/lib/{apt,dpkg,cache,log}

# Download & check & deploy dokuwiki & cleanup
RUN wget -q -O /dokuwiki.tgz "http://download.dokuwiki.org/src/dokuwiki/dokuwiki-$DOKUWIKI_VERSION.tgz" && \
    if [ "$DOKUWIKI_CSUM" != "$(md5sum /dokuwiki.tgz | awk '{print($1)}')" ];then echo "Wrong md5sum of downloaded file!"; exit 1; fi && \
    mkdir /dokuwiki && \
    tar -zxf dokuwiki.tgz -C /dokuwiki --strip-components 1 && \
    rm dokuwiki.tgz

# Set up ownership
RUN chown -R www-data:www-data /dokuwiki

# Configure lighttpd
ADD dokuwiki.conf /etc/lighttpd/conf-available/20-dokuwiki.conf
RUN lighty-enable-mod dokuwiki fastcgi accesslog
RUN mkdir /var/run/lighttpd && chown www-data.www-data /var/run/lighttpd

EXPOSE 80
VOLUME ["/dokuwiki/data/","/dokuwiki/lib/plugins/","/dokuwiki/conf/","/dokuwiki/lib/tpl/","/var/log/"]

CMD service php7.0-fpm start && /usr/sbin/lighttpd -D -f /etc/lighttpd/lighttpd.conf
# ENTRYPOINT ["/usr/sbin/lighttpd", "-D", "-f", "/etc/lighttpd/lighttpd.conf"]
